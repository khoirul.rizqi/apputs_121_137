package khoirul.rizqi.apputs_121_137

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.detail.*

class DetailData : AppCompatActivity(){

    lateinit var nama : TextView
    lateinit var anggota : TextView
    lateinit var usia : TextView
    lateinit var lat : TextView
    lateinit var lng : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.detail)
    }

    fun showData(){
        nama = findViewById(R.id.DNama)
        anggota = findViewById(R.id.DAnggota)
        usia = findViewById(R.id.DUsia)
        lat = findViewById(R.id.DLat)
        lng = findViewById(R.id.DLng)
        val bundle = intent.extras
        if (bundle != null) {
            nama.setText(bundle.getString("nama"))
            anggota.setText(bundle.getString("anggota"))
            usia.setText(bundle.getString("usia"))
            lat.setText(bundle.getString("lat"))
            lng.setText(bundle.getString("lng"))
        }
    }

    override fun onStart() {
        super.onStart()
        showData()
    }
}