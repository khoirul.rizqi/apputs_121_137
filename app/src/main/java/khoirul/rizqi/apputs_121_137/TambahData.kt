package khoirul.rizqi.apputs_121_137

import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.SimpleAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.tambahdata.*
import mumayank.com.airlocationlibrary.AirLocation

class TambahData : AppCompatActivity(), View.OnClickListener {
    var airLoc : AirLocation? = null
    val COLLECTION = "data"
    val F_ID = "id"
    val F_NAME = "name"
    val F_ANGGOTA = "anggota"
    val F_USIA  = "usia"
    val F_LAT = "lat"
    val F_LONG = "long"
    lateinit var db : FirebaseFirestore
    lateinit var alData : ArrayList<HashMap<String,Any>>
    lateinit var adapter : SimpleAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tambahdata)

        airLoc = AirLocation(this,true,true,object : AirLocation.Callbacks{
            override fun onFailed(locationFailedEnum: AirLocation.LocationFailedEnum) {

            }

            override fun onSuccess(location: Location) {
                edLng.setText("${location.longitude}")
                edLat.setText("${location.latitude}")
            }
        })

        btnTData.setOnClickListener(this)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        airLoc?.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int,permissions: Array<out String>,grantResults: IntArray) {
        airLoc?.onRequestPermissionsResult(requestCode, permissions, grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if(e!=null) Log.d("firestore", e.message.toString())
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnTData ->{
                val hm = HashMap<String,Any>()
                val lat = edLat.text.toString()
                val lng = edLng.text.toString()
                hm.set(F_ID,"$lat,$lng")
                hm.set(F_NAME,edNama2.text.toString())
                hm.set(F_ANGGOTA,edJumlah2.text.toString())
                hm.set(F_USIA,edB202.text.toString())
                hm.set(F_LAT,edLat.text.toString())
                hm.set(F_LONG,edLng.text.toString())
                db.collection(COLLECTION).document("$lat,$lng").set(hm).addOnSuccessListener {
                    Toast.makeText(this,"Data Sukses Ditambah",Toast.LENGTH_SHORT).show()
                }.addOnFailureListener { e->
                    Toast.makeText(this,"Data Gagal Ditambah : ${e.message}",Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}