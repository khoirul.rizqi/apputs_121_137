package khoirul.rizqi.apputs_121_137

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.SimpleAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.android.synthetic.main.listdata.*
import kotlinx.android.synthetic.main.row_data.*

class ListData : AppCompatActivity(), View.OnClickListener {
    val COLLECTION = "data"
    val F_ID = "id"
    val F_NAME = "name"
    val F_ANGGOTA = "anggota"
    val F_USIA  = "usia"
    val F_LAT = "lat"
    val F_LONG = "long"
    var docId = "doc"
    var nama = ""
    var anggota = ""
    var usia = ""
    var lat = ""
    var lng = ""
    lateinit var db : FirebaseFirestore
    lateinit var alData : ArrayList<HashMap<String,Any>>
    lateinit var adapter : SimpleAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.listdata)

        alData = ArrayList()
        BtnDelete.setOnClickListener(this)
        btnDetail.setOnClickListener(this)
        btnTrack.setOnClickListener(this)
        lsData.setOnItemClickListener(itemClick)
    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if(e!=null) Log.d("firestore", e.message.toString())
            showData()
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnDetail->{
                val bundle = Bundle()
                bundle.putString("nama",nama)
                bundle.putString("anggota",anggota)
                bundle.putString("usia",usia)
                bundle.putString("lat",lat)
                bundle.putString("lng",lng)
//                Toast.makeText(this,"$nama, $anggota, $usia, $lat, $lng", Toast.LENGTH_SHORT).show()
                val iD = Intent(this,DetailData::class.java)
                iD.putExtras(bundle)
                startActivity(iD)
            }
            R.id.btnTrack->{
                val bundle = Bundle()
                bundle.putString("nama",nama)
                bundle.putString("anggota",anggota)
                bundle.putString("usia",usia)
                bundle.putString("lat",lat)
                bundle.putString("lng",lng)
                val iT = Intent(this,MapData::class.java)
                iT.putExtras(bundle)
                startActivity(iT)
            }
            R.id.BtnDelete->{
                db.collection(COLLECTION).whereEqualTo(F_ID,docId).get().addOnSuccessListener {
                        results ->
                    for(doc in results){
                        db.collection(COLLECTION).document(doc.id).delete()
                            .addOnSuccessListener {
                                Toast.makeText(this,"Data Successfully Deleted",Toast.LENGTH_SHORT).show()
                            }.addOnFailureListener {e ->
                                Toast.makeText(this, "Data Unsuccessfully Deleted : ${e.message}",Toast.LENGTH_SHORT).show()
                            }
                    }
                }.addOnFailureListener { e ->
                    Toast.makeText(this, "Can't Get Data's References : ${e.message}",Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    fun showData(){
        db.collection(COLLECTION).get().addOnSuccessListener { result: QuerySnapshot? ->
            alData.clear()
            for (doc in result!!){
                val hm = HashMap<String,Any>()
                hm.set(F_ID,doc.get(F_ID).toString())
                hm.set(F_NAME,doc.get(F_NAME).toString())
                hm.set(F_ANGGOTA,doc.get(F_ANGGOTA).toString())
                hm.set(F_USIA,doc.get(F_USIA).toString())
                hm.set(F_LAT,doc.get(F_LAT).toString())
                hm.set(F_LONG,doc.get(F_LONG).toString())
                alData.add(hm)
            }
            adapter = SimpleAdapter(this,alData,R.layout.row_data,arrayOf(F_NAME,F_ANGGOTA,F_USIA,F_LAT,F_LONG),intArrayOf(R.id.txNama,R.id.txJumlah,R.id.txUsia,R.id.txLat,R.id.txLng))
            lsData.adapter = adapter
        }
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alData.get(position)
        docId = hm.get(F_ID).toString()
        txViewId.setText(docId)
        nama = hm.get(F_NAME).toString()
        anggota = hm.get(F_ANGGOTA).toString()
        usia = hm.get(F_USIA).toString()
        lat = hm.get(F_LAT).toString()
        lng = hm.get(F_LONG).toString()
    }

}