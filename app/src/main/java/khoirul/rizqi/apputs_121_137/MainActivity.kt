package khoirul.rizqi.apputs_121_137

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        TData.setOnClickListener(this)
        LData.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.TData ->{
                var intent = Intent(this,TambahData::class.java)
                startActivity(intent)
            }
            R.id.LData ->{
                var intent = Intent(this,ListData::class.java)
                startActivity(intent)
            }
        }
    }

    override fun onStart() {
        super.onStart()
    }
}